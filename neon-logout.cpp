#include <QApplication>
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <cstdlib>
#include <iostream>
#include <pwd.h>
#include <string>
#include <unistd.h>
#define sys std::system
#define str std::string

void runCommand(str command) { std::system(command.c_str()); }




  QLabel makelabel(str text, QWidget win )
  {
	QLabel lbl=new QLabel(text, win);
	lbl->setStyleSheet("QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
	lbl->setAlignment(QT::AlignCenter);
	return lbl;
  }



int main(int argc, char **argv) {

  str homedir;
  if ((homedir = getenv("HOME")) == "") {homedir = getpwuid(getuid())->pw_dir;}

  str confdir = homedir + str("/.config/neon-logout/");
  str icondir = confdir + str("icons/");
  
  // str path = str("/opt/neon-logout");

  // std::cout<<confdir<<std::endl;

  QApplication app(argc, argv);
  QWidget win;
  win.setFixedSize(1000, 150);

  QGridLayout *lay = new QGridLayout(&win);
  lay->setSpacing(0);
  lay->setContentsMargins(10, 10, 10, 10);

  QPushButton *Btn1 = new QPushButton(&win);
  QObject::connect(Btn1, SIGNAL(clicked()), &app, SLOT(quit()));
  // Btn1->setIcon(QIcon("/opt/neon-logout-git/cancel.png"));
  Btn1->setIcon(QIcon(QString::fromStdString(icondir + "cancel.png")));
  Btn1->setIconSize(QSize(100, 100));
  Btn1->setStyleSheet("QPushButton{border-radius:50%;}");
  lay->addWidget(Btn1, 0, 0, 1, 1);

  QLabel *Label1 = new QLabel("Cancel", &win);
  Label1->setStyleSheet(
	  "QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
  Label1->setAlignment(Qt::AlignCenter);
  lay->addWidget(Label1, 1, 0, 1, 1);

  QPushButton *Btn2 = new QPushButton(&win);
  QObject::connect(Btn2, &QPushButton::clicked,
				   [=]() { //runCommand(str("shutdown -h now"));
					 runCommand(str("~/.config/neon-logout/shutdown"));
				   });
  // Btn2->setIcon(QIcon("/opt/neon-logout-git/shutdown.png"));
  Btn2->setIcon(QIcon(QString::fromStdString(icondir + "shutdown.png")));
  Btn2->setIconSize(QSize(100, 100));
  Btn2->setStyleSheet("QPushButton{border-radius:50%;}");
  lay->addWidget(Btn2, 0, 1, 1, 1);

  QLabel *Label2 = new QLabel("Shutdown", &win);
  Label2->setStyleSheet(
	  "QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
  Label2->setAlignment(Qt::AlignCenter);
  lay->addWidget(Label2, 1, 1, 1, 1);

  QPushButton *Btn3 = new QPushButton(&win);
  QObject::connect(Btn3, &QPushButton::clicked,
				   [=]() {// runCommand(str("loginctl kill-user $USER"));
					 runCommand(str("~/.config/neon-logout/logout $USER"));
				   });
  //  Btn3->setIcon(QIcon("/opt/neon-logout-git/logout.png"));
  Btn3->setIcon(QIcon(QString::fromStdString(icondir + "logout.png")));
  Btn3->setIconSize(QSize(100, 100));
  Btn3->setStyleSheet("QPushButton{border-radius:50%;}");
  lay->addWidget(Btn3, 0, 2, 1, 1);

  QLabel *Label3 = new QLabel("Logout", &win);
  Label3->setStyleSheet(
	  "QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
  Label3->setAlignment(Qt::AlignCenter);
  lay->addWidget(Label3, 1, 2, 1, 1);

  QPushButton *Btn4 = new QPushButton(&win);
  QObject::connect(Btn4, &QPushButton::clicked,
				   [=]() { //runCommand(str("reboot"));
				   runCommand(str("~/.config/neon-logout/reboot"));
				   });
  // Btn4->setIcon(QIcon("/opt/neon-logout-git/reboot.png"));
  Btn4->setIcon(QIcon(QString::fromStdString(icondir + "reboot.png")));
  Btn4->setIconSize(QSize(100, 100));
  Btn4->setStyleSheet("QPushButton{border-radius:50%;}");
  lay->addWidget(Btn4, 0, 3, 1, 1);

  QLabel *Label4 = new QLabel("Reboot", &win);
  Label4->setStyleSheet(
	  "QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
  Label4->setAlignment(Qt::AlignCenter);
  lay->addWidget(Label4, 1, 3, 1, 1);

  QPushButton *Btn5 = new QPushButton(&win);
  QObject::connect(Btn5, &QPushButton::clicked, [=]() {
	runCommand(str("~/.config/neon-logout/lockscreen"));
  });
  // Btn5->setIcon(QIcon("/opt/neon-logout-git/lock.png"));
  Btn5->setIcon(QIcon(QString::fromStdString(icondir + "lock.png")));
  Btn5->setIconSize(QSize(100, 100));
  Btn5->setStyleSheet("QPushButton{border-radius:50%;}");
  lay->addWidget(Btn5, 0, 4, 1, 1);

  QLabel *Label5 = new QLabel("Lock", &win);
  Label5->setStyleSheet(
	  "QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
  Label5->setAlignment(Qt::AlignCenter);
  lay->addWidget(Label5, 1, 4, 1, 1);




  /*lock and sleep*/
   QPushButton *Btn6 = new QPushButton(&win);
   QObject::connect(Btn6, &QPushButton::clicked,
				   [=]() {
					 runCommand(str("~/.config/neon-logout/lockscreen&"));
					 runCommand(str("~/.config/neon-logout/sleep"));
				   });
   // Btn6->setIcon(QIcon("/opt/neon-logout-git/hibernate.png"));
   Btn6->setIcon(QIcon(QString::fromStdString(icondir + "secret.png")));
   Btn6->setIconSize(QSize(100, 100));
   Btn6->setStyleSheet("QPushButton{border-radius:50%;}");
   lay->addWidget(Btn6, 0, 5, 1, 1);

   QLabel *Label6 = new QLabel("lock and sleep", &win);
   Label6->setStyleSheet(
						"QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
   Label6->setAlignment(Qt::AlignCenter);
   lay->addWidget(Label6, 1, 5, 1, 1);



   /*sleep*/
  QPushButton *Btn7 = new QPushButton(&win);
  QObject::connect(Btn7, &QPushButton::clicked,
				   [=]() { //runCommand(str("systemctl suspend"));
					 runCommand(str("~/.config/neon-logout/sleep"));
				   });
  // Btn7->setIcon(QIcon("/opt/neon-logout-git/sleep.png"));
  Btn7->setIcon(QIcon(QString::fromStdString(icondir + "sleep.png")));
  Btn7->setIconSize(QSize(100, 100));
  Btn7->setStyleSheet("QPushButton{border-radius:50%;}");
  lay->addWidget(Btn7, 0, 6, 1, 1);

  QLabel *Label7 = new QLabel("Sleep", &win);
  Label7->setStyleSheet(
	  "QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
  Label7->setAlignment(Qt::AlignCenter);
  lay->addWidget(Label7, 1, 6, 1, 1);






  
  QPushButton *Btn8 = new QPushButton(&win);
  QObject::connect(Btn8, &QPushButton::clicked,
				   [=]() { //runCommand(str("systemctl hybrid-sleep"));
					 runCommand(str("~/.config/neon-logout/hybridSleep"));
				   });
  // Btn8->setIcon(QIcon("/opt/neon-logout-git/hybrid.png"));
  Btn8->setIcon(QIcon(QString::fromStdString(icondir + "hybrid.png")));
  Btn8->setIconSize(QSize(100, 100));
  Btn8->setStyleSheet("QPushButton{border-radius:50%;}");
  lay->addWidget(Btn8, 0, 7, 1, 1);

  // QLabel *Label8 = new QLabel("Hybrid Sleep", &win);
  // Label8->setStyleSheet(
  // 	  "QLabel{font-weight:600;letter-spacing:1px;font-size:15px;}");
  // Label8->setAlignment(Qt::AlignCenter);
  // lay->addWidget(Label8, 1, 7, 1, 1);

  QLabel testlabel=makelabel("hybrid sleep",&win);
  lay->addWidget(testlabel,1,7,1,1);


  
  win.show();
  return app.exec();
}
